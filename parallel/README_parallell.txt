Bruken av 'parallell_main' fungerer akkurat som beskrevet i oppgavetesten og
på samme måte som du har gjort i tilbakemeldingen på Devilry.

usage:
$ mpirun -np number_of_procs ./parallel_main kappa iters infile outfile

Sample run:
"""
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ ls  # before compiling
Makefile  parallel_main.c  README
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ make  # compile
make -C ../simple-jpeg
make[1]: Entering directory `/home/paulmag/Dropbox/kurs_uio/inf3380/oblig1/simple-jpeg'
make[1]: Nothing to be done for `all'.
make[1]: Leaving directory `/home/paulmag/Dropbox/kurs_uio/inf3380/oblig1/simple-jpeg'
mpicc -O2   -c -o parallel_main.o parallel_main.c
mpicc -O2 parallel_main.o -o parallel_main -L../simple-jpeg/ -lsimplejpeg
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ ls  # after compiling
Makefile  parallel_main  parallel_main.c  parallel_main.o  README
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ ls ../data/  # before running program
mona_lisa_noisy.jpg
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ mpirun -np 4 ./parallel_main 0.1 20 ../data/mona_lisa_noisy.jpg ../data/mona_lisa_clean.jpg  # run parallell program
paulmag@Phoenix:~/Dropbox/kurs_uio/inf3380/oblig1/parallel$ ls ../data/  # after running program
mona_lisa_clean.jpg  mona_lisa_noisy.jpg
"""

Dette fungerer på begge mine personlige PC-er, ein stasjonær og ein laptop.
Denne testkøyringa er fra laptopen. Men nå har eg også testa akkurat det same
programmet på ein UiO-maskin, og der fungerer det IKKJE. Beskrivelse av
problemet på UiO-maskin:

Når programmet køyres (på akkurat samme måte som i eksempelet over) så
starter det, men det blir ALDRI ferdig. Det klarer kun å terminere dersom eg
kommenterer ut hovedfunksjonen iso_diffusion_denoising. Det krasjer altså
ikkje slik som du opplever, men berre henger for alltid.

Dette problemet skjer altså kun på UiO-maskinen. Då eg skreiv og testa
programmet brukte eg mine eigen PC, og der fungerte det og fungerer fortsatt
som vist i Sample run over.

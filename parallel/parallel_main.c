#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/* Define a new struct type */
typedef struct {
    float** image_data; // a 2D array of floats
    int m; // # pixels in y-direction
    int n; // # pixels in x-direction
} Image;

void allocate_image(Image *u, int m, int n) {
    u->m = m;
    u->n = n;
    u->image_data = malloc((m+2)*sizeof(float*));
    int i;
    for(i=0; i<m+2; i++) {
        u->image_data[i] = malloc(n*sizeof(float));
    }
}
void deallocate_image(Image *u) {
    int i;
    for(i=0; i<u->m+2; i++) {
        free(u->image_data[i]);
    }
    free(u->image_data);
}

void convert_jpeg_to_image(const unsigned char* image_chars, Image *u) {
    int i, j;
    for(i=0; i<u->m+2; i++) {
        for(j=0; j<u->n; j++) {
            u->image_data[i][j] = (float) image_chars[i*u->n+j];
        }
    }
}
void convert_image_to_jpeg(const Image *u, unsigned char* image_chars) {
    int i, j;
    for(i=0; i<u->m; i++) {
        for(j=0; j<u->n; j++) {
            image_chars[i*u->n+j] = (char) u->image_data[i][j];
        }
    }
}

void iso_diffusion_denoising(
    Image *u, Image *u_bar, float kappa, int iters,
    int my_rank, int num_procs
) {
    int k, i, j;
    for(k=0; k<iters; k++) {
        for(i=1; i<u->m+1; i++) {
            for(j=1; j<u->n-1; j++) {
                u_bar->image_data[i][j] = u->image_data[i][j] +
                    kappa * (
                        + u->image_data[i-1][j]
                        + u->image_data[i][j-1]
                        - u->image_data[i][j] * 4
                        + u->image_data[i][j+1]
                        + u->image_data[i+1][j]
                    );
            }
        }
        if (my_rank < num_procs-1) {  // All procs but last one trade downwards.
            MPI_Send(
                u_bar->image_data[u->m],
                u->n,
                MPI_FLOAT,
                my_rank+1,  // destination rank
                0,
                MPI_COMM_WORLD
            );
            MPI_Recv(
                u_bar->image_data[u->m+1],
                u->n,
                MPI_FLOAT,
                my_rank+1,  // source rank
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        }
        if (my_rank > 0) {  // All procs but first one trade upwards.
            MPI_Send(
                u_bar->image_data[1],
                u->n,
                MPI_FLOAT,
                my_rank-1,  // destination rank
                0,
                MPI_COMM_WORLD
            );
            MPI_Recv(
                u_bar->image_data[0],
                u->n,
                MPI_FLOAT,
                my_rank-1,  // source rank
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
        }
        if (k==iters-1) {return;}
        for(i=0; i<u->m; i++) {  // Copy image back to u for next iteration.
            for(j=0; j<u->n; j++) {
                u->image_data[i][j] = u_bar->image_data[i][j];
            }
        }
    }
}

int main(int argc, char *argv[])
{
    int m, n;
    int my_m, my_n;
    int c;
    int iters;
    float kappa;
    Image u;
    Image u_bar;
    Image u_whole;
    unsigned char *image_chars;
    unsigned char *my_image_chars;
    char *input_filename;
    char *output_filename;
    int my_rank, num_procs;

    /* Initialize MPI: */
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size (MPI_COMM_WORLD, &num_procs);

    /* Read from command line:
     * kappa, iters, input_filename, output_filename
     * Use default values if cmd-line args not given by user.
     */
    if(argc >= 5) {
        kappa = atof(argv[1]);
        iters = atoi(argv[2]);
        input_filename = argv[3];
        output_filename = argv[4];
    }
    else if(argc >= 3) {
        kappa = 0.1;
        iters = 1;
        input_filename = argv[1];
        output_filename = argv[2];
    }
    else {
        kappa = 0.1;
        iters = 1;
        input_filename = "../data/mona_lisa_noisy.jpg";
        output_filename = "../data/mona_lisa_clean.jpg";
    }

    if (my_rank == 0) {
        import_JPEG_file(input_filename, &image_chars, &m, &n, &c);
        allocate_image (&u_whole, m, n);
    }

    MPI_Bcast (&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast (&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

    /* Divide the m rows evenly among the MPI processes: */
    my_m = (m-2) / num_procs;
    my_n = n;
    my_image_chars = malloc((my_m+2)*my_n*sizeof(char));

    allocate_image (&u, my_m, my_n);
    allocate_image (&u_bar, my_m, my_n);

    /* Each process asks process 0 for a partitioned region */
    /* of image_chars and copy the values into u: */
    if (my_rank == 0) {
        int rank, i, j;
        for (rank=1; rank<num_procs; rank++) {
            for (i=0; i<my_m+2; i++) {
                for (j=0; j<my_n; j++) {
                    my_image_chars[i*my_n+j] =
                        image_chars[rank*my_m*my_n + i*my_n+j];
                }
            }
            MPI_Send(
                my_image_chars,
                (my_m+2) * my_n,
                MPI_BYTE,
                rank,  // destination rank
                0,
                MPI_COMM_WORLD
            );
        }
    }
    else if (my_rank > 0) {
        MPI_Recv(
            my_image_chars,
            (my_m+2) * my_n,
            MPI_BYTE,
            0,  // source rank
            0,
            MPI_COMM_WORLD,
            MPI_STATUS_IGNORE
        );
    }

    /* Make partition for self (rank 0) */
    if (my_rank == 0) {
        int i, j;
        for (i=0; i<my_m+2; i++) {
            for (j=0; j<my_n; j++) {
                my_image_chars[i*my_n+j] = image_chars[i*my_n+j];
            }
        }
    }

    convert_jpeg_to_image (my_image_chars, &u);
    iso_diffusion_denoising (&u, &u_bar, kappa, iters, my_rank, num_procs);

    /* Each process sends its resulting content of u_bar to process 0 */
    /* process 0 receives from each process incoming values and */
    /* copy them into the designated region of struct u_whole: */
    if (my_rank > 0) {
        MPI_Send(
            *u_bar.image_data,
            (my_m+2) * my_n,
            MPI_FLOAT,
            0,  // destination rank
            0,
            MPI_COMM_WORLD
        );
    }
    else if (my_rank == 0) {
        int rank, i, j;
        for (i=0; i<my_m+1; i++) {  // Rank 0 copy from self.
            for (j=0; j<my_n; j++) {
                u_whole.image_data[i][j] = u_bar.image_data[i][j];
            }
        }
        for (rank=1; rank<num_procs; rank++) {  // Receive from the others.
            MPI_Recv(
                *u_bar.image_data,
                (my_m+2) * my_n,
                MPI_FLOAT,
                rank,  // source rank
                0,
                MPI_COMM_WORLD,
                MPI_STATUS_IGNORE
            );
            for (i=1; i<my_m+1; i++) {
                for (j=0; j<my_n; j++) {
                    u_whole.image_data[rank*my_m+i][j] = u_bar.image_data[i][j];
                }
            }
        }
    }

    if (my_rank==0) {
        convert_image_to_jpeg(&u_whole, image_chars);
        export_JPEG_file(output_filename, image_chars, m, n, c, 75);

        free(image_chars);
        deallocate_image (&u_whole);
    }
    free(my_image_chars);
    deallocate_image (&u);
    deallocate_image (&u_bar);
    MPI_Finalize ();

    return 0;
}

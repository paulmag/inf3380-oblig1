#include <stdio.h>
#include <stdlib.h>

/* Define a new struct type */
typedef struct {
    float** image_data; // a 2D array of floats
    int m; // # pixels in y-direction
    int n; // # pixels in x-direction
} Image;

void allocate_image(Image *u, int m, int n) {
    u->m = m;
    u->n = n;
    u->image_data = malloc(m*sizeof(float*));
    int i;
    for(i=0; i<m; i++) {
        u->image_data[i] = malloc(n*sizeof(float));
    }
}
void deallocate_image(Image *u) {
    int i;
    for(i=0; i<u->m; i++) {
        free(u->image_data[i]);
    }
    free(u->image_data);
}

void convert_jpeg_to_image(const unsigned char* image_chars, Image *u) {
    int i, j;
    for(i=0; i<u->m; i++) {
        for(j=0; j<u->n; j++) {
            u->image_data[i][j] = (float) image_chars[i*u->n+j];
        }
    }
}
void convert_image_to_jpeg(const Image *u, unsigned char* image_chars) {
    int i, j;
    for(i=0; i<u->m; i++) {
        for(j=0; j<u->n; j++) {
            image_chars[i*u->n+j] = (char) u->image_data[i][j];
        }
    }
}

void iso_diffusion_denoising(Image *u, Image *u_bar, float kappa, int iters) {
    int k, i, j;
    for(k=0; k<iters; k++) {
        for(i=1; i<u->m-1; i++) {
            for(j=1; j<u->n-1; j++) {
                u_bar->image_data[i][j] = u->image_data[i][j] +
                    kappa * (
                        + u->image_data[i-1][j]
                        + u->image_data[i][j-1]
                        - 4 * u->image_data[i][j]
                        + u->image_data[i][j+1]
                        + u->image_data[i+1][j]
                    );
            }
        }
        if (k==iters-1) {return;}
        for(i=0; i<u->m; i++) {  // Copy image back to u for next iteration.
            for(j=0; j<u->n; j++) {
                u->image_data[i][j] = u_bar->image_data[i][j];
            }
        }
    }
}

int main(int argc, char *argv[])
{
    int m, n;
    int c;
    int iters;
    float kappa;
    Image u;
    Image u_bar;
    unsigned char *image_chars;
    char *input_filename;
    char *output_filename;

    /* Read from command line:
     * kappa, iters, input_filename, output_filename
     * Use default values if cmd-line args not given by user.
     */
    if(argc >= 5) {
        kappa = atof(argv[1]);
        iters = atoi(argv[2]);
        input_filename = argv[3];
        output_filename = argv[4];
    }
    else if(argc >= 3) {
        kappa = 0.1;
        iters = 1;
        input_filename = argv[1];
        output_filename = argv[2];
    }
    else {
        kappa = 0.1;
        iters = 1;
        input_filename = "../data/mona_lisa_noisy.jpg";
        output_filename = "../data/mona_lisa_clean.jpg";
    }

    import_JPEG_file(input_filename, &image_chars, &m, &n, &c);
    allocate_image (&u, m, n);
    allocate_image (&u_bar, m, n);
    convert_jpeg_to_image (image_chars, &u);
    iso_diffusion_denoising (&u, &u_bar, kappa, iters);
    convert_image_to_jpeg (&u_bar, image_chars);
    export_JPEG_file(output_filename, image_chars, m, n, c, 75);

    free(image_chars);
    deallocate_image (&u);
    deallocate_image (&u_bar);
    return 0;
}
